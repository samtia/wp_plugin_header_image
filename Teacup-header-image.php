<?php

/*
Plugin Name: Teacup Header Image
Plugin URI: https://www.fiverr.com/atiqsamtia
Description: Plugin for Showing Header Image at http://www.teacupmaltesepups.com/
Version: 1.0
Author: Atiq Samtia
Author URI: https://www.fiverr.com/atiqsamtia
*/



register_activation_hook(__file__, 'falcons_fiverr_install');

function falcons_fiverr_install(){

    add_option('falcons_fiverr_image_path','http://www.teacupmaltesepups.com/wp-content/uploads/2016/02/ExtendedMaltesebannerphotoshop.png','','yes');
}


if (is_admin()) {

    add_action('admin_menu', 'falcons_fiverr_admin_add');
    add_action('admin_init', 'falcons_fiverr_reg');


    function falcons_fiverr_admin_add()
    {
        add_menu_page('Header Image', 'Header Image', 'manage_options', 'falcons_fiverr_header_image',
            'falcons_fiverr_admin_main');
        add_submenu_page('falcons_fiverr_header_image', 'Settings', 'Settings', 'manage_options',
            'falcons_fiverr_header_image_settings', 'falcons_fiverr_admin_settings');

    }

    function falcons_fiverr_reg()
    { // whitelist options
        register_setting('falcons_fiverr-group', 'falcons_fiverr_image_path');

    }

}

function falcons_fiverr_admin_main(){

    ?>

    <div class="wrap">
        <h2>
            Falcons Header Image
        </h2>

        <p>
            Simple Plugin for Fiverr Client by Fiverr.com/atiqsamtia
        </p>

    </div>

   <?php
}


function falcons_fiverr_admin_settings(){

    if (isset($_POST['falcons_fiverr_image_path'])) {
        update_option(falcons_fiverr_image_path, $_POST['falcons_fiverr_image_path']);

    }
    ?>

    <div class="wrap">
        <h2>
            Falcons Header Image
        </h2>
        <form method="post" action="options.php">
            <?php settings_fields( 'falcons_fiverr-group'); ?>
            <?php do_settings_sections( 'falcons_fiverr-group'); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">
                       Image Path
                    </th>
                    <td>
                        <input type="text" size="100" name="falcons_fiverr_image_path" value="<?php echo get_option('falcons_fiverr_image_path'); ?>" />
                        <br/> (Upload image in Media and Paste the Path here)
                    </td>
                    </td>
                </tr>

            </table>
            <?php submit_button(); ?>
        </form>
    </div>

    <?php

}





// load jQuery
wp_enqueue_script('jquery');

// hook the widget area into the footer, then move into place in the header
function falcons_fiverr_add_header_img() { ?>
    <div style="display:none">
        <img id="falcons_fiverr-page-start-img" src="<?php echo get_option('falcons_fiverr_image_path'); ?>"/>
    </div>
    <script>jQuery("#falcons_fiverr-page-start-img").prependTo(jQuery("body"));</script>
    <?php
}
add_action('wp_footer', 'falcons_fiverr_add_header_img');

// CSS to display the image and header correctly
function falcons_fiverr_css() { ?>
    <style>
        /* Change header to float correctly wherever it is in the page */
        @media only screen and ( min-width:500px ) {
            #main-header { position:relative !important; top:0px !important; }
            #main-header.et-fixed-header { position:fixed !important; margin-bottom:0px; top:0px !important; }
            body.admin-bar #main-header.et-fixed-header { top:32px !important; }
            #page-container { overflow:hidden; }
        }

        /* Style the image for full screen layouts */
        @media only screen and ( min-width:981px ) {
            #falcons_fiverr-page-start-img { margin-bottom:0px; width:100%;}
        }

        /* Style the image for box layout */
        @media only screen and (min-width: 981px) {
            .et_boxed_layout #falcons_fiverr-page-start-img {
                width:981px;position:relative !important; margin-bottom:-97px; top:0px !important;
                -moz-box-shadow: 0 0 0px 0 rgba(0,0,0,0.2);
                -webkit-box-shadow: 0 0 0px 0 rgba(0, 0, 0, 0.2);
                box-shadow: 0 0 0px 0 rgba(0, 0, 0, 0.2);
                display:block !important;
                margin-left:auto;
                margin-right:auto;

            }

    </style>
    <?php
}
add_action('wp_head', 'falcons_fiverr_css');